package com.jfalco.gbmchallenge.repository;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.Condition;
import com.amazonaws.services.dynamodbv2.model.ComparisonOperator;
import com.jfalco.gbmchallenge.thirdparty.DynamoDBClient;
import com.jfalco.gbmchallenge.entity.Car;
import com.jfalco.gbmchallenge.entity.CarPositionHistory;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import javax.inject.Inject;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;

/**
 *
 * @author javie
 */
@Log4j2
@RequiredArgsConstructor(onConstructor = @_(
        @Inject))
public class CarRepository {

    @NonNull
    private DynamoDBClient dynamoDBClient;

    public Car save(final Car car) {
        dynamoDBClient.getDynamoDBMapper().save(car);
        return car;
    }

    public CarPositionHistory save(final CarPositionHistory carPositionHistory) {
        dynamoDBClient.getDynamoDBMapper().save(carPositionHistory);
        return carPositionHistory;
    }

    public Optional<Car> read(final String vin) {
        return Optional.ofNullable(dynamoDBClient.getDynamoDBMapper().load(Car.class, vin));
    }

    public Optional<CarPositionHistory> readHistory(final String vin, final Date date) {
        return Optional.ofNullable(dynamoDBClient.getDynamoDBMapper().load(CarPositionHistory.class, vin, date));
    }

    public Set<CarPositionHistory> listHistory(final String vin) {
        DynamoDBScanExpression dynamoDBScanExpression = new DynamoDBScanExpression();

        Map<String, Condition> filterCondition = new HashMap<>();
        filterCondition.put("vin",
                new Condition()
                        .withComparisonOperator(ComparisonOperator.EQ)
                        .withAttributeValueList(
                                new AttributeValue().withS(vin)));
        
        dynamoDBScanExpression.withScanFilter(filterCondition);
        
        return dynamoDBClient.getDynamoDBMapper().scan(CarPositionHistory.class, 
                dynamoDBScanExpression).stream().collect(Collectors.toSet());
    }
}
