package com.jfalco.gbmchallenge.repository;

import com.amazonaws.services.sns.model.MessageAttributeValue;
import com.amazonaws.services.sns.model.PublishRequest;
import com.amazonaws.services.sns.model.PublishResult;
import com.amazonaws.services.sns.model.SubscribeRequest;
import com.amazonaws.services.sns.model.SubscribeResult;
import com.jfalco.gbmchallenge.entity.CarNotificationSubscription;
import com.jfalco.gbmchallenge.thirdparty.SNSClient;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import javax.inject.Inject;
import jersey.repackaged.com.google.common.collect.ImmutableMap;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;

/**
 *
 * @author javie
 */
@Log4j2
@RequiredArgsConstructor(onConstructor = @_(
        @Inject))
public class CarSubscriptionRepository {

    @NonNull
    private SNSClient snsClient;
    private final String topicArn = "arn:aws:sns:us-west-2:839575687633:test";

    public PublishResult publish(final CarNotificationSubscription carNotificationSubscription, final String message) {
        final PublishRequest publishRequest = new PublishRequest(topicArn, message);
        final Map<String, MessageAttributeValue> messageAttributes = new HashMap<>();
        final MessageAttributeValue messageAttributeValue = new MessageAttributeValue()
                .withDataType("String")
                .withStringValue(carNotificationSubscription.getVin());
        messageAttributes.put("vin", messageAttributeValue);
        publishRequest.setMessageAttributes(messageAttributes);
        return snsClient.getSNSClient().publish(publishRequest);
    }
    
    public SubscribeResult subscribe(final CarNotificationSubscription carNotificationSubscription) {
        final Optional<String> maybeEmail = Optional.ofNullable(carNotificationSubscription.getEmail());
        SubscribeResult subscribeResult = null;

        if (maybeEmail.isPresent()) {
            final String email = maybeEmail.get();
            final SubscribeRequest subscribeRequest = new SubscribeRequest(topicArn, "email", email);
            final String filterPolicy = "{\"vin\":[\"" + carNotificationSubscription.getVin() + "\"]}";
            final Map<String, String> attributesMap
                    = new ImmutableMap.Builder<String, String>()
                            .put("FilterPolicy", filterPolicy)
                            .build();
            subscribeRequest.setAttributes(attributesMap);

            subscribeResult = snsClient.getSNSClient().subscribe(subscribeRequest);
            log.info(subscribeResult);
        }
        
        return subscribeResult;
    }
}
