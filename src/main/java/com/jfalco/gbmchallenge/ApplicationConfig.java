/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jfalco.gbmchallenge;

import com.jfalco.gbmchallenge.thirdparty.DynamoDBClient;
import com.jfalco.gbmchallenge.repository.CarRepository;
import com.jfalco.gbmchallenge.repository.CarSubscriptionRepository;
import com.jfalco.gbmchallenge.thirdparty.SNSClient;
import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.glassfish.jersey.server.ResourceConfig;

/**
 *
 * @author javie
 */
@javax.ws.rs.ApplicationPath("/")
public class ApplicationConfig extends ResourceConfig {

    public ApplicationConfig() {
        register(CarsResource.class);
        register(CarsHistoryResource.class);
        register(CarsNotificationResource.class);
        register(new AbstractBinder() {
            @Override
            protected void configure() {
                bindAsContract(DynamoDBClient.class);
                bindAsContract(CarRepository.class);
                bindAsContract(SNSClient.class);
                bindAsContract(CarSubscriptionRepository.class);
            }
        });
    }
}
