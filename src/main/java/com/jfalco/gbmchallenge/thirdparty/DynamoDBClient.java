package com.jfalco.gbmchallenge.thirdparty;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;

/**
 *
 * @author javie
 */
public class DynamoDBClient {
    
    private DynamoDBMapper dynamoDBMapper;
    private final static String ACCESS_KEY = "AKIAJETIBPO62XIFD3TA";
    private final static String SECRET_KEY = "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";
    
    public DynamoDBMapper getDynamoDBMapper(){
        BasicAWSCredentials awsCreds = new BasicAWSCredentials(ACCESS_KEY, SECRET_KEY);
        AmazonDynamoDB dynamoDBClient = 
                AmazonDynamoDBClientBuilder
                        .standard()
                        .withRegion(Regions.US_WEST_2)
                        .withCredentials(new AWSStaticCredentialsProvider(awsCreds)).build();
        return new DynamoDBMapper(dynamoDBClient);
    }
}
