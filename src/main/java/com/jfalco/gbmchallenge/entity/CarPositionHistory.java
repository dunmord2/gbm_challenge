
package com.jfalco.gbmchallenge.entity;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAutoGeneratedKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAutoGeneratedTimestamp;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBRangeKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
/**
 *
 * @author javie
 */
@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString
@DynamoDBTable(tableName = "car_position_history")
public class CarPositionHistory {
    
    @DynamoDBAutoGeneratedKey
    @DynamoDBHashKey(attributeName = "uuid")
    private String id;
    
    @DynamoDBRangeKey(attributeName = "vin")
    private String vin;
    
    @DynamoDBAutoGeneratedTimestamp
    @DynamoDBAttribute(attributeName = "date")
    private Date date;
    
    @DynamoDBAttribute(attributeName = "latitude")
    private Long latitude;
    
    @DynamoDBAttribute(attributeName = "longitude")
    private Long longitude;
    
    
}
