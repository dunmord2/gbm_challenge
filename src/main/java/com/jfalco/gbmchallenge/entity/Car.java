
package com.jfalco.gbmchallenge.entity;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
/**
 *
 * @author javie
 */
@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString
@DynamoDBTable(tableName = "cars")
public class Car {
    
    @DynamoDBHashKey(attributeName = "vin")
    private String vin;
    
    @DynamoDBAttribute(attributeName = "latitude")
    private Long latitude;
    
    @DynamoDBAttribute(attributeName = "longitude")
    private Long longitude;
    
    
}
