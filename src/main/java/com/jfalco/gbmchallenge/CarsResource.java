package com.jfalco.gbmchallenge;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jfalco.gbmchallenge.entity.Car;
import com.jfalco.gbmchallenge.entity.CarNotificationSubscription;
import com.jfalco.gbmchallenge.entity.CarPositionHistory;
import com.jfalco.gbmchallenge.repository.CarRepository;
import com.jfalco.gbmchallenge.repository.CarSubscriptionRepository;
import java.io.IOException;
import javax.inject.Inject;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import lombok.extern.log4j.Log4j2;

/**
 * REST Web Service
 *
 * @author javie
 */
@Log4j2
@Path("cars/{vin}")
public class CarsResource {

    @Inject
    private CarRepository carRepository;

    @Inject
    private CarSubscriptionRepository carSubscriptionRepository;

    @Context
    private UriInfo context;

    private final static ObjectMapper jsonMapper = new ObjectMapper();

    /**
     * Creates a new instance of CarsResource
     */
    public CarsResource() {
    }

    /**
     * Retrieves representation of an instance of
     * com.jfalco.gbmchallenge.CarsResource
     *
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getJson(@PathParam("vin") String vin) {
        try {
            return jsonMapper.writeValueAsString(carRepository.read(vin).orElse(Car.builder().build()));
        } catch (JsonProcessingException jsonProcessingException) {
            log.error(jsonProcessingException);
        }
        return "";
    }

    /**
     * PUT method for updating or creating an instance of CarsResource
     *
     * @param content representation for the resource
     */
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public Response putJson(String content, @PathParam("vin") String vin) {
        Car car = null;
        try {
            car = jsonMapper.readValue(content, Car.class);
        } catch (IOException ioex) {
            log.error(ioex);
            Response.status(Response.Status.BAD_REQUEST).build();
        }
        car.setVin(vin);
        carRepository.save(car);
        saveHistory(car);
        notifySubscribers(car, vin);
        
        return Response.ok().build();
    }

    private void notifySubscribers(final Car latestCarData, final String vin) {
        CarNotificationSubscription carNotificationSubscription
                = CarNotificationSubscription.builder()
                        .vin(vin)
                        .build();
        String message = "";
        try {
            message = jsonMapper.writeValueAsString(latestCarData);
        } catch (IOException ioex) {
            log.error(ioex);
        }
        carSubscriptionRepository.publish(carNotificationSubscription, message);
    }

    private void saveHistory(final Car car) {
        final CarPositionHistory carPositionHistory
                = CarPositionHistory
                        .builder()
                        .vin(car.getVin())
                        .latitude(car.getLatitude())
                        .longitude(car.getLongitude())
                        .build();
        carRepository.save(carPositionHistory);
    }
}
