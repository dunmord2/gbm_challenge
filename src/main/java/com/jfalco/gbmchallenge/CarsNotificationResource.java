/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jfalco.gbmchallenge;

import com.amazonaws.services.sns.model.SubscribeResult;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jfalco.gbmchallenge.entity.CarNotificationSubscription;
import com.jfalco.gbmchallenge.repository.CarSubscriptionRepository;
import java.io.IOException;
import java.util.Optional;
import javax.inject.Inject;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import lombok.extern.log4j.Log4j2;

/**
 * REST Web Service
 *
 * @author javie
 */
@Log4j2
@Path("cars/{vin}/subscribe")
public class CarsNotificationResource {
    
    @Inject
    private CarSubscriptionRepository carSubscriptionRepository;
    
    @Context
    private UriInfo context;
   
    private final static ObjectMapper jsonMapper = new ObjectMapper();
    
    /**
     * Creates a new instance of CarsResource
     */
    public CarsNotificationResource() {
    }
    
    /**
     * PUT method for updating or creating an instance of CarsNotificationResource
     * @param content representation for the resource
     */
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public Response putJson(String content, @PathParam("vin") String vin) {
        try {
            CarNotificationSubscription carNotificationSubscription = jsonMapper.readValue(content, CarNotificationSubscription.class);
            carNotificationSubscription.setVin(vin);
            SubscribeResult result = carSubscriptionRepository.subscribe(carNotificationSubscription);
            return Response.ok().build();
        } catch(IOException ioex) {
            log.error(ioex);
        }
        return Response.ok().build();
    }
}
