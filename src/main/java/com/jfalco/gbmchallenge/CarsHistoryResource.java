package com.jfalco.gbmchallenge;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jfalco.gbmchallenge.entity.Car;
import com.jfalco.gbmchallenge.entity.CarPositionHistory;
import com.jfalco.gbmchallenge.repository.CarRepository;
import java.io.IOException;
import java.util.HashSet;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import lombok.extern.log4j.Log4j2;

/**
 *
 * @author javie
 */
@Log4j2
@Path("cars/{vin}/history")
public class CarsHistoryResource {

    @Inject
    private CarRepository carRepository;

    @Context
    private UriInfo context;

    private final static ObjectMapper jsonMapper = new ObjectMapper();

    /**
     * Creates a new instance of CarsHistoryResource
     */
    public CarsHistoryResource() {
    }

    /**
     * Retrieves representation of an instance of
     * com.jfalco.gbmchallenge.CarsResource
     *
     * @return an instance of java.lang.String
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getJson(@PathParam("vin") String vin) {
        try {
            return jsonMapper.writeValueAsString(carRepository.listHistory(vin));
        } catch (JsonProcessingException jsonProcessingException) {
            log.error(jsonProcessingException);
        }
        return "";
    }

    /**
     * PUT method for updating or creating an instance of CarsResource
     *
     * @param content representation for the resource
     */
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public Response putJson(String content, @PathParam("vin") String vin) {
        try {
            CarPositionHistory carPositionHistory = jsonMapper.readValue(content, CarPositionHistory.class);
            carPositionHistory.setVin(vin);
            carRepository.save(carPositionHistory);
        } catch (IOException ioex) {
            log.error(ioex);
        }
        return Response.ok().build();
    }
}
