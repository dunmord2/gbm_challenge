package com.jfalco.gbmchallenge.repository;

import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.model.SubscribeResult;
import com.jfalco.gbmchallenge.entity.CarNotificationSubscription;
import com.jfalco.gbmchallenge.thirdparty.SNSClient;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.Matchers.any;
import org.mockito.Mock;
import org.mockito.Mockito;
import static org.mockito.Mockito.when;
import org.mockito.MockitoAnnotations;

/**
 *
 * @author javie
 */
public class CarSubscriptionRepositoryTest {

    @Mock
    private SNSClient snsClient;

    @Mock
    private AmazonSNS amazonSNS;

    private CarSubscriptionRepository carSubscriptionRepository;
    private final static String TOPIC_ARN = "arn:aws:sns:us-west-2:123455687633:test";
    private final static String VIN = "carvin";
    private final static String email = "somemail@email.com";
    private final static String SUBSCRIPTION1_ARN = "arn:aws:sns:us-west-2:123455687633:test/some_subscription";

    @Before
    public void populateTestData() {
        MockitoAnnotations.initMocks(this);
        Mockito.when(snsClient.getSNSClient()).thenReturn(amazonSNS);

        when(amazonSNS.subscribe(any())).thenReturn(getGoodResult());
        carSubscriptionRepository = new CarSubscriptionRepository(snsClient);
    }

    @Test
    public void testSubscribeNoErrors() {
        CarNotificationSubscription carNotificationSubscription
                = CarNotificationSubscription.builder()
                        .vin(VIN)
                        .email(email)
                        .build();

        final SubscribeResult subscribeResult
                = carSubscriptionRepository.subscribe(carNotificationSubscription);
        assertNotNull(subscribeResult.getSubscriptionArn());
    }

    @Test
    public void testSubscribeReturnsNull() {
        CarNotificationSubscription carNotificationSubscription
                = CarNotificationSubscription.builder()
                        .vin(VIN)
                        .build();

        final SubscribeResult subscribeResult
                = carSubscriptionRepository.subscribe(carNotificationSubscription);
        assertNull(subscribeResult);
    }

    private SubscribeResult getGoodResult() {
        return ((new SubscribeResult()).withSubscriptionArn(SUBSCRIPTION1_ARN));
    }
}
