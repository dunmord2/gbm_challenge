package com.jfalco.gbmchallenge.repository;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.jfalco.gbmchallenge.entity.Car;
import com.jfalco.gbmchallenge.entity.CarPositionHistory;
import com.jfalco.gbmchallenge.thirdparty.DynamoDBClient;
import java.util.Date;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import static org.mockito.Mockito.when;
import org.mockito.MockitoAnnotations;

/**
 *
 * @author javie
 */
public class CarRepositoryTest {

    @Mock
    private DynamoDBClient dynamoDBClient;

    @Mock
    private DynamoDBMapper dynamoDBMapper;

    private CarRepository carRepository;

    private static final String VIN = "testvin";
    private static final long LATITUDE = 1234;
    private static final long LONGITUDE = 9876;
    private static final long LATITUDE2 = 5678;
    private static final long LONGITUDE2 = 5432;
    private static final Date DATE = new Date(System.currentTimeMillis());
    private static final Date DATE2 = new Date(System.currentTimeMillis());

    @Before()
    public void populateTestData() {
        MockitoAnnotations.initMocks(this);
        when(dynamoDBClient.getDynamoDBMapper()).thenReturn(dynamoDBMapper);
        Car car
                = Car.builder()
                        .vin(VIN)
                        .latitude(LATITUDE)
                        .longitude(LONGITUDE)
                        .build();
        CarPositionHistory carPositionHistory
                = CarPositionHistory.builder()
                        .vin(VIN)
                        .latitude(LATITUDE)
                        .longitude(LONGITUDE)
                        .date(DATE)
                        .build();
        CarPositionHistory carPositionHistory2
                = CarPositionHistory.builder()
                        .vin(VIN)
                        .latitude(LATITUDE2)
                        .longitude(LONGITUDE2)
                        .date(DATE2)
                        .build();

        when(dynamoDBMapper.load(Car.class, VIN)).thenReturn(car);
        when(dynamoDBMapper.load(CarPositionHistory.class, VIN, DATE)).thenReturn(carPositionHistory);

        carRepository = new CarRepository(dynamoDBClient);
    }

    @Test
    public void testSaveCarIsSuccessful() {
        Car car = Car.builder()
                .vin(VIN)
                .latitude(LATITUDE)
                .longitude(LONGITUDE)
                .build();
        carRepository.save(car);
    }

    @Test
    public void testSaveCarPositionHistoryIsSuccessful() {
        CarPositionHistory carPositionHistory
                = CarPositionHistory.builder()
                        .vin(VIN)
                        .latitude(LATITUDE)
                        .longitude(LONGITUDE)
                        .date(DATE)
                        .build();
        carRepository.save(carPositionHistory);
    }
    
    @Test
    public void testReadCarIsSuccessful() {
        Car car = carRepository.read(VIN).get();
        assertEquals((long)car.getLatitude(), LATITUDE);
    }
    
    @Test
    public void testReadCarFails() {
        final String ANOTHER_VIN = "anothervin";
        
        Car car = carRepository.read(ANOTHER_VIN).orElse(null);
        assertNull(car);
    }
    
    @Test
    public void testReadCarHistoryIsSuccessful() {
        CarPositionHistory carPositionHistory
                = carRepository.readHistory(VIN, DATE).get();
        assertEquals((long)carPositionHistory.getLatitude(), LATITUDE);
    }
    
    @Test
    public void testReadCarHistoryFails() {
        final String ANOTHER_VIN = "anothervin";
        CarPositionHistory carPositionHistory
                = carRepository.readHistory(ANOTHER_VIN, DATE).orElse(null);
        assertNull(carPositionHistory);
        
    }
}
